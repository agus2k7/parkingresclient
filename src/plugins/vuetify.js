import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
// import es from 'vuetify/src/locale/es.ts'
import es from '../i18n/vuetify/es.ts'

Vue.use(Vuetify, {
  iconfont: 'md',
  lang: {
    locales: { es },
    current: 'es'
  }
})
