import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import TipoVehiculos from './views/TipoVehiculos.vue'
import Tarifas from './views/Tarifas.vue'
import Ejemplo from './views/Ejemplo.vue'
import TipoVehiculosOld from './views/TipoVehiculosOld.vue'
import Test from './views/Test.vue'
import Marcas from './views/Marcas.vue'
import Modelos from './views/Modelos.vue'
import Estadias from './views/Estadias.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/tipovehiculosold',
      name: 'tipovehiculosold',
      component: TipoVehiculosOld
    },
    {
      path: '/tarifas',
      name: 'tarifas',
      component: Tarifas
    },
    {
      path: '/ejemplo',
      name: 'ejemplo',
      component: Ejemplo
    },
    {
      path: '/tipovehiculos',
      name: 'tipovehiculos',
      component: TipoVehiculos
    },
    {
      path: '/test',
      name: 'test',
      component: Test
    },
    {
      path: '/marcas',
      name: 'marcas',
      component: Marcas
    },
    {
      path: '/modelos',
      name: 'modelos',
      component: Modelos
    },
    {
      path: '/estadias',
      name: 'estadias',
      component: Estadias
    }
  ]
})
