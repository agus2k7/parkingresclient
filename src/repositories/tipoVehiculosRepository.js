import Repository from './Repository'

const resource = '/tipovehiculos'

export default {
    get(){
        return Repository.get(`${resource}`)
    },
    post(payload){
        return Repository.post(`${resource}`, payload)
    },
    put(id, payload){
        return Repository.put(`${resource}/${id}`, payload)
    },
    getById(id){
        return Repository.get(`${resource}/${id}`)
    },
    delete(id){
        return Repository.delete(`${resource}/${id}`)
    }
}