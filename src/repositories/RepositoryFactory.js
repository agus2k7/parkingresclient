import TipoVehiculosRepository from './tipoVehiculosRepository'
import TarifasRepository from './tarifasRepository'
import MarcasRepository from './marcasRepository'
import ModelosRepository from './modelosRepository'
import EstadiasRepository from './estadiasRepository'
import TurnosRepository from './turnosRepository'
import ClientesRepository from './clientesRepository'

const repositories = {
    tipovehiculos: TipoVehiculosRepository,
    tarifas: TarifasRepository,
    marcas: MarcasRepository,
    modelos: ModelosRepository,
    estadias: EstadiasRepository,
    turnos: TurnosRepository,
    clientes: ClientesRepository
}

export const RepositoryFactory = {
    get: name => repositories[name]
}