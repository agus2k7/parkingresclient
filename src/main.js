import Vue from 'vue'
import './plugins/vuetify'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import Notifications from 'vue-notification'
import moment from 'moment'
// import VuetifyDialog from 'vuetify-dialog'
// Vue.use(VuetifyDialog)

Vue.use(Notifications)

Vue.prototype.moment = moment

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
